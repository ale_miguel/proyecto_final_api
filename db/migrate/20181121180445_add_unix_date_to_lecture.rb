class AddUnixDateToLecture < ActiveRecord::Migration[5.2]
  def change
    add_column :lectures, :unix_date, :integer
end
end
