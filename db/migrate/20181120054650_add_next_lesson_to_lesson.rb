class AddNextLessonToLesson < ActiveRecord::Migration[5.2]
  def change
    add_column :lessons, :next_lesson_id, :integer
  end
end
