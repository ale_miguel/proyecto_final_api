class ChangeToStringMaxStudentFromRule < ActiveRecord::Migration[5.2]
  def change
  	remove_column :rules, :max_students
  	add_column :rules, :max_students, :integer
  end
end
