class AddPhoneAndEmailToSubscribers < ActiveRecord::Migration[5.2]
  def change
  	add_column :subscribers, :email, :string
  	add_column :subscribers, :phone, :string
  end
end
