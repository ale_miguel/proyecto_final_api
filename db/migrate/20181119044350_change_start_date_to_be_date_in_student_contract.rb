class ChangeStartDateToBeDateInStudentContract < ActiveRecord::Migration[5.2]
  def up
  	change_column :student_contracts, :start_date, :date
  end
end
