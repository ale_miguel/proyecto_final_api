class AddTestGradeToStudent < ActiveRecord::Migration[5.2]
  def self.up
    add_column :students, :test_grade, :integer
  end
  def self.down
  	add_column :students, :test_grade, :integer
  end
end
