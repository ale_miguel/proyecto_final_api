class RenameColumnReservations < ActiveRecord::Migration[5.2]
  def change
  	rename_column :reservations, :class_id, :lecture_id
  end
end
